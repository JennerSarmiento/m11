# 1) Creació del certificat i la key del CA (autosignada)
openssl req -new -x509 -nodes -sha1 -days 365 -keyout ca.key -out ca.crt
```
Country                     ca
State                       ca
Local                       bcn
Organizational Name         VeritatAbsolutaJennerSarmiento
Organizational Unit Name    edt
DNS                         ldap.edt.org
Email                       ldap@edt.inf.com
```
#### *COMPROVACIÓ*
openssl x509 -noout -text -in ca.crt



# 2) Creem la request del servidor y el serverkey (Per després fer la petició del CA)
openssl req -new -nodes -keyout serverkey.ldap.pem -out serverreq.ldap.csr

#### *COMPROVACIÓ*
openssl req -noout -text -in serverreq.ldap.csr



# 3) Demana el servidor al CA que es crei el certificat signat per el CA
openssl x509 -CA ca.crt -CAkey ca.key -req -in serverreq.ldap.csr -days 365 -extfile ca.conf -CAcreateserial -out servercert.ldap.crt

#### *COMPROVACIO*
openssl x509 -noout -text -in servercert.ldap.crt

# 4) Creem el docker build

docker build -t jennersarmiento/tls23:examen .

# 5) Iniciem el docker

docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -p 636:636 -d jennersarmiento/tls23:examen

# 6) Entrem al docker a fer les proves desde dins:
docker exec -it ldap.edt.org /bin/bash

# 7) Comprovacions dintre del docker

ldapsearch -x -LLL -H ldap://ldap.edt.org -s base -b dc="edt,dc=org" dn
ldapsearch -x -LLL -H ldaps://ldap.edt.org -s base -b dc="edt,dc=org" dn    
ldapsearch -x -LLL -Z -H ldap://ldap.edt.org -s base -b dc="edt,dc=org" dn 
ldapsearch -x -LLL -ZZ -H ldap://ldap.edt.org -s base -b dc="edt,dc=org" dn 

LOCAL

# 8) Modificació del /etc/host (IP i el FQDN)
/etc/host
172.17.0.2 ldap.edt.org jennersarmiento.edt.org

# 9)Modificació del local a /etc/ldap/ldap.conf (indicar el crt del CA)
TLS_CACERT <dir del ca.crt>


# 9) Comprovacions desde el ordinador al ldap.edt.org

```
ldapsearch -x -LLL -h 10.200.245.216 -s base -b "dc=edt,dc=org" dn
ldapsearch -x -LLL -H ldaps://10.200.245.216 -s base -b "dc=edt,dc=org" dn
ldapsearch -x -Z -LLL -h 10.200.245.216 -s base -b "dc=edt,dc=org" dn
```

### Comprovacions examen:
```
ldapsearch -H ldaps://ldap.edt.org -s base -b "dc=edt,dc=org" dn
ldapsearch -ZZ -h localhost -s base -b "dc=edt,dc=org" dn
ldapsearch -ZZ -h 172.17.0.2 -s base -b "dc=edt,dc=org" dn
ldapsearch -ZZ -h jennersarmiento.edt.org -s base -b "dc=edt,dc=org" dn
ldapsearch -d1 -h localhost -s base -b "dc=edt,dc=org" dn | less
```

```
ldapsearch -x -LLL -H ldaps://ldap.edt.org -s base -b "dc=edt,dc=org" dn
ldapsearch -x -LLL -ZZ -h localhost -s base -b "dc=edt,dc=org" dn
ldapsearch -x -LLL -ZZ -h 172.17.0.2 -s base -b "dc=edt,dc=org" dn
ldapsearch -x -LLL -ZZ -h jennersarmiento.edt.org -s base -b "dc=edt,dc=org" dn
ldapsearch -x -LLL -d1 -h localhost -s base -b "dc=edt,dc=org" dn | less

```

10.200.245.216