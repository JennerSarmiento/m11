# ¡¡¡ WIPE OUT DE LES PARTICIONS APAGAR I MOSTRAR QUE FUNCIONA EL RAID !!!

#Pràctica (2) REAL

## a. Crear sda2: 10G, sda3: 4G, sda4: 4G

```
Creació sda2,3 i 4:
$ sudo fdisk /dev/sda:
  - p
  - n
  - p
  - \n (enter)
  - +10G or +4G
  - w

Comprobació
$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 232.9G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda2   8:2    0    10G  0 part 
├─sda3   8:3    0     4G  0 part 
├─sda4   8:4    0     4G  0 part 
├─sda5   8:5    0  90.3G  0 part /
├─sda6   8:6    0  90.3G  0 part 
└─sda7   8:7    0   4.7G  0 part [SWAP]

```

## b. Crear un raid1 amb sda3 i sda4
```
$ sudo mdadm --create /dev/md/raid1 --level=1 --raid-devices=2 /dev/sda3 /dev/sda4

$ sudo cat /proc/mdstat
Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] 
md127 : active raid1 sda4[1] sda3[0]
      4189184 blocks super 1.2 [2/2] [UU]
      
unused devices: <none>

```

## c. generar un VG anomenat diskedt amb sda3 i sda4
```
$ sudo vgcreate diskedt /dev/md/raid1
$ sudo vgdisplay diskedt
  --- Volume group ---
  VG Name               diskedt
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               3.99 GiB
  PE Size               4.00 MiB
  Total PE              1022
  Alloc PE / Size       0 / 0   
  Free  PE / Size       1022 / 3.99 GiB
  VG UUID               Tem2u2-T2CX-UJOq-OH97-MMeX-uiqP-fEx7CD

```

## d. generar lv sistema de 3G, Dades de 1G
```
$ sudo lvcreate -L 3G -n sistema /dev/diskedt
$ sudo lvcreate -l100%FREE -n dades /dev/diskedt

$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/diskedt/sistema
  LV Name                sistema
  VG Name                diskedt
  LV UUID                u47qNr-c72g-4EcV-2kqZ-dkhi-PFRC-CN6FL4
  LV Write Access        read/write
  LV Creation host, time g16, 2023-03-10 10:35:14 +0100
  LV Status              available
  # open                 0
  LV Size                3.00 GiB
  Current LE             768
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/diskedt/dades
  LV Name                dades
  VG Name                diskedt
  LV UUID                WRg0qL-OnZy-36XV-85Sk-CMeY-HVsK-M5fh8e
  LV Write Access        read/write
  LV Creation host, time g16, 2023-03-10 10:37:27 +0100
  LV Status              available
  # open                 0
  LV Size                1016.00 MiB
  Current LE             254
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1

```

## e. muntar i posar-hi xixa.

```
$ sudo mkfs -t ext4 /dev/diskedt/sistema
$ sudo mkfs -t ext4 /dev/diskedt/dades
$ sudo mount /dev/diskedt/sistema /mnt/sistema
$ sudo mount /dev/diskedt/dades /mnt/dades

$ sudo cp /etc/os-release /mnt/sistema/
$ sudo cp /etc/group /mnt/dades/


$ df -h
df: /run/user/204971/doc: Operation not permitted
Filesystem                   Size  Used Avail Use% Mounted on
udev                         3.9G     0  3.9G   0% /dev
tmpfs                        787M  1.9M  786M   1% /run
/dev/sda5                     89G   81G  3.1G  97% /
tmpfs                        3.9G  300M  3.6G   8% /dev/shm
tmpfs                        5.0M  4.0K  5.0M   1% /run/lock
tmpfs                        787M  140K  787M   1% /run/user/204971
/dev/mapper/diskedt-sistema  2.9G   28K  2.8G   1% /mnt/sistema
/dev/mapper/diskedt-dades    982M   28K  915M   1% /mnt/dades
```

## f. automatitzar el muntatge (/mnt/dades, /mnt/sistema)

```
$ sudo cat /etc/fstab 
/dev/diskedt/sistema	/mnt/sistema	ext4	errors=remount-ro	0	0
/dev/diskedt/dades	/mnt/dades	ext4	errors=remount-ro	0	0

```

## g. reboot i verificar que dades i sistema estan disponibles.