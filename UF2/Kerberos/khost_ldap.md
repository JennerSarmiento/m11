# Autenticar

#### kerberos22:kserver
#### ldap:latest

#### kerberos22:khost

Instalar els paquets necessaris per poder atacar contra el ldap:
```
apt-get install libpam-krb5 libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
```
### Install mode:
```
su URI es --> ldap://ldap.edt.org
LDAP server search base --> dc=edt,dc=org
Name services to configure: 1 2
```
## /etc/ldap/ldap.conf
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```
## /etc/nslcd.conf
```
nmap ldap.edt.org
ldapsearch -x

# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org

# The search base that will be used for all queries.
base dc=edt,dc=org

# The LDAP protocol version to use.
#ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
#bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com

# SSL options
#ssl off
#tls_reqcert never
tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub
```

## /etc/nsswitch.conf 
```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
```

### Iniciar els daemons
```
service nscd start
```
```
service nslcd start
```

# Entrem com a unix01 (agafa les dades per ldap):
```
su - unix01
su - pere (pw: kpere)
klist
cntl+D
```

# Al posar otra passwd intenta identificar com a ldap i kerberos (hem de modificar el PAM):
```
su - unix01
su - pere (pw: kpere)
klist
cntl+D
```

# Observar els common del PAM (tècnicament el fa de manera auto al instalar packets)
### /etc/pam.d/common-auth
```
# /etc/pam.d/common-auth - authentication settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authentication modules that define
# the central authentication scheme for use on the system
# (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
# traditional Unix authentication mechanisms.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
auth	[success=3 default=ignore]	pam_krb5.so minimum_uid=1000
auth	[success=2 default=ignore]	pam_unix.so nullok try_first_pass
auth	[success=1 default=ignore]	pam_ldap.so minimum_uid=1000 use_first_pass
# here's the fallback if no module succeeds
auth	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
auth	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
auth	optional	pam_mount.so 
auth	optional			pam_cap.so 
# end of pam-auth-update config
```

PROFILES /usr/share/pam-configs --> Tipos de configs de pam que tenim
```
root@khost:/opt/docker# tree /usr/share/pam-configs/
/usr/share/pam-configs/
|-- capability
|-- krb5
|-- ldap
|-- libpam-mount
|-- mkhomedir
`-- unix
```
root@khost:/opt/docker# pam-auth-update --enable unix krb5 --remove ldap
root@khost:/opt/docker# apt-get install dialog

## Comprovem que posant SOLAMENT la passwd de Kerberos et deixa entrar.
```
# pam-auth-update --enable mkhomedir unix krb5 pwquality
# su - unix01
Creating directory '/home/unix01'.
$ su pere
Password: kpere
$ klist
Ticket cache: FILE:/tmp/krb5cc_5001_R8X7iE
Default principal: pere@EDT.ORG

Valid starting     Expires            Service principal
03/27/23 08:32:03  03/27/23 18:32:03  krbtgt/EDT.ORG@EDT.ORG
	renew until 03/28/23 08:32:03
$ pwd
/home/unix01
```

# ASEGURAR:
nslcd.conf nsswitch.conf ldap.conf krb5.conf install.sh startup.sh
### install.sh
```
#! /bin/bash
# Kserver
# @edt ASIX M11-SAD Curs 2022-2023

cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

# Usuaris purs unix, amb /etc/passwd IP i AP
for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done
```
### startup.sh
```
#! /bin/bash

/opt/docker/install.sh && echo "Ok install"
nscd
nslcd
pam-auth-update --enable mkhomedir pwquality unix libpam-mount krb5 --remove ldap
/bin/bash
```