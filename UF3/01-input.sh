#! /bin/bash
# @jennersarmiento ASIX M11 2022-2023
# IPTABLES
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush (tirar cadena)
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT


# Obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Obrir la nostra ip
iptables -A INPUT -s 10.200.245.216 -j ACCEPT
iptables -A INPUT -d 10.200.245.216 -j ACCEPT

# Exemple regles input
#===============================================
# Obrir a tothom el port 13
iptables -A INPUT -p tcp --dport 13 -j ACCEPT

# Tancar a tothom el port 2013
iptables -A INPUT -p tcp --dport 2013 -j REJECT

# Tancar a tothom el port 3013 amb drop
iptables -A INPUT -p tcp --dport 3013 -j DROP

# Tancar el g25 el port 4013 però obert a tothom
iptables -A INPUT -p tcp --dport 4013 -s 10.200.245.225 -j DROP
iptables -A INPUT -p tcp --dport 4013 -j ACCEPT

# Port tancat a tothom, però obert a hisx2 però tancat a g25
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.225 -j REJECT
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.0/24 -j ACCEPT
iptables -A INPUT -p tcp --dport 5013 -j DROP

# Port 6013, obert tothom, tancat aula, obert g25
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.225/32 -j ACCEPT
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.0/24 -j REJECT
iptables -A INPUT -p tcp --dport 6013 -j ACCEPT

# Tancar accés als ports del 3000 al 8000
#iptables -A INPUT -p tcp --dport 3000:8000 -j REJECT

# Barrera final (tancar ports 0:1023)
#iptables -A INPUT -p tcp --dport 1:1024 -j REJECT

