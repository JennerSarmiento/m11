# ¡¡¡ WIPE OUT DE LES PARTICIONS APAGAR I MOSTRAR QUE FUNCIONA EL RAID !!!

# Pràctica (1) RAID+LVM  en loops

## a. raid1 dos discs de 500M.  
```
$ dd if=/dev/zero of=disk01.img bs=1k count=500k status=progress 
$ dd if=/dev/zero of=disk02.img bs=1k count=500k status=progress 


$ sudo losetup /dev/loop1 disk01.img 
$ sudo losetup /dev/loop2 disk02.img 
$ sudo losetup -a

$ sudo mdadm --create /dev/md/a --level=1 --raid-devices=2 /dev/loop1 /dev/loop2
$ cat /proc/mdstat 
Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] 
md127 : active raid1 loop2[1] loop1[0]
      510976 blocks super 1.2 [2/2] [UU]
      
unused devices: <none>


$ sudo mdadm --detail /dev/md/a
/dev/md/a:
           Version : 1.2
     Creation Time : Fri Mar 10 09:10:34 2023
        Raid Level : raid1
        Array Size : 510976 (499.00 MiB 523.24 MB)
     Used Dev Size : 510976 (499.00 MiB 523.24 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Fri Mar 10 09:10:36 2023
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : g16:a  (local to host g16)
              UUID : 6ea7ef76:073fe09c:f04ac622:7ecec6af
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       7        1        0      active sync   /dev/loop1
       1       7        2        1      active sync   /dev/loop2

```

## b. Un segon Raid1 (dos discs de 500M).
```
$ dd if=/dev/zero of=disk03.img bs=1k count=500k status=progress 
$ dd if=/dev/zero of=disk04.img bs=1k count=500k status=progress 

$ sudo losetup /dev/loop3 disk03.img 
$ sudo losetup /dev/loop4 disk04.img 
$ sudo losetup -a

$ sudo mdadm --create /dev/md/b --level=1 --raid-devices=2 /dev/loop3 /dev/loop4

$ cat /proc/mdstat
Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] 
md126 : active raid1 loop4[1] loop3[0]
      510976 blocks super 1.2 [2/2] [UU]
      
md127 : active raid1 loop2[1] loop1[0]
      510976 blocks super 1.2 [2/2] [UU]
      
unused devices: <none>


$ sudo mdadm --detail /dev/md/b
/dev/md/b:
           Version : 1.2
     Creation Time : Fri Mar 10 09:20:49 2023
        Raid Level : raid1
        Array Size : 510976 (499.00 MiB 523.24 MB)
     Used Dev Size : 510976 (499.00 MiB 523.24 MB)
      Raid Devices : 2
     Total Devices : 2
       Persistence : Superblock is persistent

       Update Time : Fri Mar 10 09:20:52 2023
             State : clean 
    Active Devices : 2
   Working Devices : 2
    Failed Devices : 0
     Spare Devices : 0

Consistency Policy : resync

              Name : g16:b  (local to host g16)
              UUID : 2fdca688:30dcc85d:0e190192:8af5af51
            Events : 17

    Number   Major   Minor   RaidDevice State
       0       7        3        0      active sync   /dev/loop3
       1       7        4        1      active sync   /dev/loop4


```

## c. Primer raid Raid1 crear PV i VG mydisc (no incloure el segon raid).
```
$ sudo pvcreate /dev/md/a
$ sudo vgcreate mydisc /dev/md/a

```

## d. LV de 200M sistema, LV de 100M dades.
```
$ sudo lvcreate -L 200M -n sistema /dev/mydisc
Logical volume "sistema" created.
$ sudo lvcreate -L 100M -n dades /dev/mydisc
Logical volume "dades" created.

$ sudo lvdisplay /dev/mydisc/sistema
  --- Logical volume ---
  LV Path                /dev/mydisc/sistema
  LV Name                sistema
  VG Name                mydisc
  LV UUID                WJtP3A-XXrg-s3ty-tWTo-goGg-lHc5-Sj7Sbn
  LV Write Access        read/write
  LV Creation host, time g16, 2023-03-10 09:39:45 +0100
  LV Status              available
  # open                 0
  LV Size                200.00 MiB
  Current LE             50
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
$ sudo lvdisplay /dev/mydisc/dades
  --- Logical volume ---
  LV Path                /dev/mydisc/dades
  LV Name                dades
  VG Name                mydisc
  LV UUID                z9rkQY-KnVd-f2is-tdoy-ApVn-tG24-BPyR5Z
  LV Write Access        read/write
  LV Creation host, time g16, 2023-03-10 09:39:54 +0100
  LV Status              available
  # open                 0
  LV Size                100.00 MiB
  Current LE             25
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:1

```

## e. Mkfs i Muntar i posar-hi dades
```
$ sudo mkfs -t ext4 /dev/mydisc/sistema
$ sudo mkfs -t ext4 /dev/mydisc/dades

$ sudo mkdir /var/tmp/sistema
$ sudo mkdir /var/tmp/dades

$ sudo mount /dev/mydisc/sistema /var/tmp/sistema/
$ sudo mount /dev/mydisc/dades /var/tmp/dades/
$ df -h
/dev/mapper/mydisc-sistema  189M   14K  175M   1% /var/tmp/sistema
/dev/mapper/mydisc-dades     92M   14K   85M   1% /var/tmp/dades

$ mount -t ext4 | grep mydisc
/dev/mapper/mydisc-sistema on /var/tmp/sistema type ext4 (rw,relatime)
/dev/mapper/mydisc-dades on /var/tmp/dades type ext4 (rw,relatime)

```

## f. Incorporar al VG el segon raid Raid2.

```
$ sudo vgextend mydisc /dev/md/b
$ sudo vgdisplay mydisc
  --- Volume group ---
  VG Name               mydisc
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  4
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               992.00 MiB
  PE Size               4.00 MiB
  Total PE              248
  Alloc PE / Size       75 / 300.00 MiB
  Free  PE / Size       173 / 692.00 MiB
  VG UUID               xk2tuY-awcX-QwPk-C8J6-G9yd-Igve-q9xaGW

```

## g. Incrementar sistema +100M.
```
$ sudo lvextend -L +100M /dev/mydisc/sistema
$ sudo lvdisplay /dev/mydisc/sistema
  --- Logical volume ---
  LV Path                /dev/mydisc/sistema
  LV Name                sistema
  VG Name                mydisc
  LV UUID                WJtP3A-XXrg-s3ty-tWTo-goGg-lHc5-Sj7Sbn
  LV Write Access        read/write
  LV Creation host, time g16, 2023-03-10 09:39:45 +0100
  LV Status              available
  # open                 1
  LV Size                304.00 MiB
  Current LE             76
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0

```
## h. Provocar un fail a un dels disc del Raid1
```
$ sudo mdadm /dev/md/a --fail /dev/loop1
mdadm: set /dev/loop1 faulty in /dev/md/a

$ sudo cat /proc/mdstat
Personalities : [linear] [multipath] [raid0] [raid1] [raid6] [raid5] [raid4] [raid10] 
md126 : active raid1 loop4[1] loop3[0]
      510976 blocks super 1.2 [2/2] [UU]
      
md127 : active raid1 loop2[1] loop1[0](F)
      510976 blocks super 1.2 [2/1] [_U]
      
unused devices: <none>

```
## i. Eliminar completament el Raid1 del VG.
```
$ sudo umount /var/tmp/sistema
$ sudo umount /var/tmp/dades

Per poder eliminar un PV d'un VG ha d'estar lliure completament, el que fem es moure tot el que tñe la Raid1 a Raid2

$ sudo pvmove /dev/md/a /dev/md/b
$ sudo vgreduce mydisc /dev/md/a
```

# Neteja de la pràctica:
```
DESMUNTATGDE DEL MUNTATGE

umount /var/tmp/sistema
umount/var/tmp/dades

LV:

$ sudo lvremove /dev/mydisc/sistema /dev/mydisc/dades

VG:

$ sudo vgremove mydisc

ELIMINAR ELS RAIDS:

$ sudo mdadm --stop --scan
$ sudo cat /proc/mdstat 

NETEJAR MARQUES DEL RAID
$ sudo mdadm -v --zero-superblock /dev/loop{1..4}
$ sudo mdadm --examine --scan

```