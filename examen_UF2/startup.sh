#! /bin/bash

echo "Inicialització BD ldap edt.org"
# copia de certificats
cp  /opt/docker/ldap.conf /etc/ldap/ldap.conf
mkdir /etc/ldap/certs
cp /opt/docker/ca.crt /etc/ldap/certs/.
cp /opt/docker/servercert.ldap.crt /etc/ldap/certs/.
cp /opt/docker/serverkey.ldap.pem  /etc/ldap/certs/.
# Esborrar config predeterminada
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# generar nou directori de config apartir de fitxer de config
slaptest -f slapd.conf -F /etc/ldap/slapd.d
# afegir dades al dir de BD
slapadd -F /etc/ldap/slapd.d/ -l  edt-org.ldif
# cambiar permisos per fer-los del user ldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
# engegar dimoni ldap en foreground
/usr/sbin/slapd -d3 -h "ldap:/// ldaps:/// ldapi:///" && echo "slapd Ok"
