#! /bin/bash
# @jennersarmiento ASIX M11 2022-2023
#iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush (tirar cadena)
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

#politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT


#obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#obrir la nostra ip
iptables -A INPUT -s 10.200.245.216  -j ACCEPT
iptables -A INPUT -d 10.200.245.216 -j ACCEPT

# Exemples de trafic related, established

# Permetre navegar per la red (port 80 / 443)
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT

# Soc un servidor Web 
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT 

# Soc un servidor web però el g25 no ens deixa accedir 
iptables -A INPUT -p tcp --dport 80 -s 10.200.245.225 -j REJECT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT 
