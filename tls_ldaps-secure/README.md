1)Creació del certificat i la key del CA (autosignada)
openssl req -new -x509 -nodes -sha1 -days 365 -keyout ca.key -out ca.crt

openssl req -new -nodes -keyout serverkey.ldap.pem -out serverreq.ldap.csr
openssl x509 -CA ca.crt -CAkey ca.key -req -in serverreq.ldap.csr -days 365 -extfile ca.conf -CAcreateserial -out servercert.ldap.crt
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -p 636:636 -d jennersarmiento/tls23:ldaps

PROVES docker:
ldapsearch -x -LLL -H ldap://ldap.edt.org -s base -b dc="edt,dc=org" dn 
ldapsearch -x -LLL -H ldaps://ldap.edt.org -s base -b dc="edt,dc=org" dn 
ldapsearch -x -LLL -Z -H ldap://ldap.edt.org -s base -b dc="edt,dc=org" dn 
ldapsearch -x -LLL -ZZ -H ldap://ldap.edt.org -s base -b dc="edt,dc=org" dn 

LOCAL

/etc/host
<IP> ldap.edt.org ldapserver.exemple.org
ldapsearch -x -LLL -h 172.17.0.2 -s base -b "dc=edt,dc=org" dn
ldapsearch -x -LLL -H ldaps://172.17.0.2 -s base -b "dc=edt,dc=org" dn
ldapsearch -x -Z -LLL -h 172.17.0.2 -s base -b "dc=edt,dc=org" dn

/etc/ldap/ldap.conf
TLS_CACERT <dir del ca.crt>


