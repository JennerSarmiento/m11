#! /bin/bash
# @jennersarmiento ASIX M11 2022-2023
#iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# regles flush (tirar cadena)
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

#politica per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT


#obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#obrir la nostra ip
iptables -A INPUT -s 10.200.245.216  -j ACCEPT
iptables -A INPUT -d 10.200.245.216 -j ACCEPT

